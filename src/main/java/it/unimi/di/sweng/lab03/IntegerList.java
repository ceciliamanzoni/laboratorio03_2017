package it.unimi.di.sweng.lab03;

import java.util.Scanner;
import java.util.function.IntPredicate;

public class IntegerList {
	private IntegerNode mainNode;
	private IntegerNode currentNode;
	public IntegerList(){
		mainNode = null;
	}
	public IntegerList(String string) {
		Scanner sc = new Scanner(string);
		int i;
		while(sc.hasNext()){
			
			i = Integer.parseInt(sc.next());
			this.addLast(i); 
		}
			
	}
	@Override
	public String toString(){
		currentNode = mainNode;
		StringBuilder stringhetta = new StringBuilder();
		stringhetta.append("[");
		while(currentNode != null){
			stringhetta.append(currentNode.getValue());
			if(currentNode.getNext() != null){
				stringhetta.append(" ");
			}
			currentNode = currentNode.getNext();
		}stringhetta.append("]");
		return stringhetta.toString();
	}

	public void addLast(int i) {
		currentNode = mainNode;
		
		IntegerNode nuovo = new IntegerNode(i);
		if(mainNode == null){
			mainNode = nuovo;
		}else {
		while(currentNode.getNext() != null){
			currentNode = currentNode.getNext();
		}
		currentNode.setNext(nuovo);
		}
		}
	public void addFirst(int i) {
		IntegerNode nuovo = new IntegerNode(i);
		nuovo.setNext(mainNode);
		mainNode = nuovo;
		
	}
	public boolean removeFirst() {
		if(this.mainNode == null){
			return false;
		}
		this.mainNode = mainNode.getNext(); 
		return true;
	}
	public boolean removeLast() {
		IntegerNode precedente = mainNode;
		currentNode = mainNode;
		if(this.mainNode == null){
			return false;
		}
		while(currentNode.getNext() != null){
			precedente = currentNode;
			currentNode = currentNode.getNext();
		}
		precedente.setNext(null);
		if(precedente.equals(currentNode))
			this.mainNode = null;
		return true;
	}
	public boolean remove(int i) {
		IntegerNode precedente = mainNode;
		currentNode = mainNode;
		if(this.mainNode == null){
			return false;
		}
		while(currentNode.getValue() != i && currentNode.getNext() != null){
			precedente = currentNode;
			currentNode = currentNode.getNext();
		}
	
		if(precedente.equals(currentNode) && mainNode.getValue()==i){
			this.mainNode = null;
		    return true;
		} else if(currentNode.getValue()==i){
			precedente.setNext(currentNode.getNext());
			return true;
		}
		return false;
	}
		}


