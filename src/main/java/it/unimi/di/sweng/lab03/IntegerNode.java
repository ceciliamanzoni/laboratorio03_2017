package it.unimi.di.sweng.lab03;

public class IntegerNode {
private int value;
private IntegerNode next;
public IntegerNode(int i){
	this.value = i;
	this.next = null;
}
public int getValue(){
	return this.value;
}
public void setValue(int value){
	this.value = value;
}
public IntegerNode getNext(){
	return this.next;
}
public void setNext(IntegerNode next){
	this.next = next;
}
}
