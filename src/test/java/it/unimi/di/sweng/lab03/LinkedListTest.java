package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
	public void noParamConstructor() {
		list = new IntegerList();
		assertThat(list.toString()).isEqualTo("[]");
	}
	
	@Test
	public void addLastCheck() {
		list = new IntegerList();
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
	}
	
	@Test
	public void stringConstructor() {
		list = new IntegerList("");
		assertThat(list.toString()).isEqualTo("[]");
		list = new IntegerList("1");
		assertThat(list.toString()).isEqualTo("[1]");
		list = new IntegerList("1 2 3");
		assertThat(list.toString()).isEqualTo("[1 2 3]");
	}
	
	@Test
	public void exceptionConstructor() {
		try{
			list = new IntegerList("1 2 aaa");
			fail("Expected an IllegalArgumentException");
		} catch(IllegalArgumentException exc){
			assertThat(true);
		}
	}
	
	@Test
	public void addFirstCheck() {
		list = new IntegerList();
		list.addFirst(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addFirst(3);
		assertThat(list.toString()).isEqualTo("[3 1]");
	}
	
	@Test
	public void removeFirstCheck() {
		list = new IntegerList("1 2");
		assertThat(list.removeFirst());
		assertThat(list.toString()).isEqualTo("[2]");
		assertThat(list.removeFirst());
		assertThat(list.toString()).isEqualTo("[]");
		assertThat(!list.removeFirst());
	}
	
	@Test
	public void removeLastCheck() {
		list = new IntegerList("7 10");
		assertThat(list.removeLast());
		assertThat(list.toString()).isEqualTo("[7]");
		assertThat(list.removeLast());
		assertThat(list.toString()).isEqualTo("[]");
		assertThat(!list.removeLast());
	}
	
	@Test
	public void removeCheck() {
		list = new IntegerList("1 2 3 4 3 5");
		assertThat(list.remove(2));
		assertThat(list.toString()).isEqualTo("[1 3 4 3 5]");
		assertThat(list.remove(3));
		assertThat(list.toString()).isEqualTo("[1 4 3 5]");
		assertThat(!list.remove(6));
	}
}
